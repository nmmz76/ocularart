import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image, ImageOps
import os
import numpy as np
import random

TRAIN_PATH = "E:/ACIL stuff/Data/WIDER/WIDER_train/WIDER_train/images/"
TEST_PATH = "E:/ACIL stuff/Data/WIDER/WIDER_test/WIDER_test/images/"
VAL_PATH = "E:/ACIL stuff/Data/WIDER/WIDER_val/WIDER_val/images/"
TRAIN_LABEL_PATH = "E:/ACIL stuff/Data/WIDER/wider_face_split/wider_face_split/wider_face_train_bbx_gt.txt"
VAL_LABEL_PATH = "E:/ACIL stuff/Data/WIDER/wider_face_split/wider_face_split/wider_face_val_bbx_gt.txt"

LINE_DATA_KEYS = ['x','y','w','h','blur','expression','illumination','invalid','occlusion','pose']

def fetch_class_list():
    class_list = []
    classes = dict()
    max_num = -np.inf
    min_num  = np.inf
    for x in os.walk(TRAIN_PATH):
        parts = x[0].split('--')
        pnum = parts[0].split('/')[-1]
        if pnum:
            classes[pnum] = '--'.join(parts[1:])
            max_num = max(max_num,int(pnum))
            min_num = min(min_num,int(pnum))
    for i in range(max_num-min_num+1):
        try:
            class_list.append(classes[str(i)])
        except:
            # print('No Class',i)
            pass
    # return class_list
    return classes


def fetch_file_list(t='train'):
    if t == 'train':
        F = TRAIN_PATH
    elif t == 'test':
        F = TEST_PATH
    elif t == 'val':
        F = VAL_PATH
    else:
        raise ValueError('Invalid Data type {} is not train, test, or val.'.format(t))
    classes = fetch_class_list()
    class_files = dict()
    for x in os.walk(F):
        parts = x[0].split('--')
        pnum = parts[0].split('/')[-1]
        if pnum and pnum in classes:
            class_files[pnum] = [x[0]+'/'+f for f in x[2]]
    return class_files

def fetch_all_file_lists():
    files = {
        'train': fetch_file_list('train'),
        'test': fetch_file_list('test'),
        'val': fetch_file_list('val'),
    }
    return files

def fetch_labels(t):
    labels = dict()
    if t == 'train':
        f = open(TRAIN_LABEL_PATH,'r')
    elif t == 'val':
        f = open(VAL_LABEL_PATH, 'r')
    else:
        raise ValueError('Invalid Data type {} is not train or val.'.format(t))
    fname = ''
    for line in f.readlines():
        if '--' in line:
            # new file
            if t == 'train':
                fname = TRAIN_PATH+line.strip()
            else:
                fname = VAL_PATH + line.strip()
            labels[fname] = []
        else:
            line_data = line.split(' ')
            if len(line_data) > 1:
                line_data_dict = {k:int(v) for k,v in zip(LINE_DATA_KEYS,line_data)}
                labels[fname].append(line_data_dict)
    return labels


def fetch_all_labels():
    return {
        'train': fetch_labels('train'),
        'val': fetch_labels('val')
    }

def IOU(x1,y1,w1,h1,x2,y2,w2,h2):
    dx = min(x1+w1,x2+w2) - max(x1,x2)
    dy = min(y1+h1,y2+h2) - max(y1,y2)
    if dx>0 and dy>0:
        inter = dx*dy
        union = w1*h1 + w2*h2 - inter
        return inter/union
    else:
        return 0.

def generate_face_detection_data(t='train',ftnr=1.0,settings=None,shuffle_classes=False):
    if settings is None:
        settings = {'occlusion':0,'blur':0,'expression':0,'illumination':0,'pose':0,'invalid':0}
    file_class_list = fetch_file_list(t)
    file_labels = fetch_labels(t)
    file_class_index = []
    for c in file_class_list:
        idx = list(range(len(file_class_list[c])))
        np.random.shuffle(idx)
        file_class_index += [[c,i] for i in idx]
    if shuffle_classes:
        np.shuffle(file_class_index)

    for class_label, i in file_class_index:
        file_name = file_class_list[class_label][i]
        # print(file_name)
        faces = file_labels[file_name]
        data_out = []
        for face in faces:
            if all([face[k]<= v for k,v in settings.items()]):
                data_out.append([face['x'],face['y'],face['w'],face['h'],True])
        if data_out:
            img = Image.open(file_name)
            if ftnr > 0 and not np.isinf(ftnr):
                for nfi in range(int(len(data_out)/ftnr)):
                    is_face = True
                    while is_face:
                        x = int(np.random.random()*img.size[0]-16)
                        y = int(np.random.random()*img.size[1]-16)
                        r = np.random.random()**2
                        w = int((img.size[0]-x)*r)
                        h = int((img.size[1]-y)*r)
                        if img.size[0] > x >= 0 and img.size[1] > y >= 0 and img.size[0] > x + w >= 0 and \
                                img.size[1] > h + y >= 0 and w >= 16 and h >= 16 and 2.0 > h/w > 0.5:
                            is_face = False
                            for face in faces:
                                    if IOU(face['x'],face['y'],face['w'],face['h'],x,y,w,h) > 0.:
                                        is_face = True
                                        # print('is face')
                                        break
                            if not is_face:
                                data_out.append([x,y,w,h,False])
        np.random.shuffle(data_out)
        for sample in data_out:
            x1 = sample[0]
            x2 = x1 + sample[2]
            y1 = sample[1]
            y2 = y1 + sample[3]
            slice = np.array(ImageOps.grayscale(img.crop((x1,y1,x2,y2)).resize((32,32)))).flatten()
            yield slice, sample[-1], class_label, file_name, (x1,y1,sample[2],sample[3])


def batch_faces(t='train',ftnr=-1,settings=None,shuffle_classes=False):
    batch_X = []
    batch_Y = []
    batch_Z = []
    for sample, label, class_label, file_name, (x,y,w,h) in generate_face_detection_data(t,ftnr=ftnr,settings=settings,shuffle_classes=shuffle_classes):
        batch_X.append(sample)
        batch_Y.append(label)
        batch_Z.append(class_label)
    return batch_X, batch_Y, batch_Z






if __name__ == '__main__':
    X,Y,Z = batch_faces(t='val',ftnr=1.0)
    import pickle
    pickle.dump({'X':X,'Y':Y,'Z':Z},open('WIDER_FACES_W_NOISE_VAL_GRAY.pckl','wb'))
    print('complete')
    # while True:
    #     f, axarr = plt.subplots(4, 4)
    #     i = 0
    #     last_file = ''
    #     for sample, label, class_label, file_name, (x,y,w,h) in generate_face_detection_data():
    #         axarr[np.int(np.floor(i / 4)), np.int(i % 4)].imshow(sample.reshape((32, 32, 1)))
    #         axarr[np.int(np.floor(i / 4)), np.int(i % 4)].title.set_text(str(label))
    #         i += 1
    #         if i >= 16:
    #             break
    #         if file_name != last_file:
    #             fig,ax = plt.subplots(1)
    #             ax.imshow(Image.open(file_name))
    #             last_file = str(file_name)
    #         if label:
    #             rect = patches.Rectangle((x, y), w, h, linewidth=1, edgecolor='r', facecolor='none')
    #         else:
    #             rect = patches.Rectangle((x, y), w, h, linewidth=1, edgecolor='b', facecolor='none')
    #         ax.add_patch(rect)
    #     plt.show()
