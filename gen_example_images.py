import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pickle



def gen_retinal_patch_example(cx,cy):
    NUM_PERIPHERAL = 8
    R_PERIPHERAL = 4
    WINDOW_W = 10
    WINDOW_W2 = WINDOW_W/2
    IMG_S = 32
    # [ocular_X, ocular_Y] = pickle.load(open('ocular_images.pckl', 'rb'))
    fig = plt.figure()
    ax = fig.gca()
    # plt.imshow(ocular_X[0])
    img = np.ones((32,32))
    img[cy,cx] = 0
    ax.imshow(img,'gray')
    ax.set_xticks(np.arange(-0.5, 32.5, 1))
    ax.set_yticks(np.arange(-0.5, 32.5, 1))
    ax.set_xticklabels(map(str,np.arange(0,33,1)))
    ax.set_yticklabels(map(str,np.arange(0,33,1)))
    plt.grid()

    #add peripheral
    for i in range(8):
        angle = i*(2*np.pi)/NUM_PERIPHERAL
        x = np.int16(R_PERIPHERAL*np.cos(angle))+cx
        y = np.int16(R_PERIPHERAL*np.sin(angle))+cy
        print(angle,x,y)
        rx = min(max(0,x-WINDOW_W2),IMG_S)
        ry = min(max(0,y-WINDOW_W2),IMG_S)
        rw = min(10,IMG_S-rx)
        rh = min(10,IMG_S-ry)
        rect = patches.Rectangle((rx, ry), rw, rh, linewidth=3, edgecolor='r', facecolor='b',alpha=0.2)
        ax.add_patch(rect)
        # plt.scatter(float(x),float(y),c='r',marker='x')
        plt.pause(0.01)
    # central
    rect = patches.Rectangle((cx - 5, cy - 5), 10, 10, linewidth=5, edgecolor='r', facecolor='none')
    # plt.scatter(cx, cy, c='g', marker='x')
    print('CENTER', cx, cy)
    ax.add_patch(rect)
    plt.pause(0.01)
    plt.show()

if __name__ == '__main__':
    cx = np.random.randint(0+10,32-10)
    cy = np.random.randint(0+10,32-10)
    gen_retinal_patch_example(cx,cy)