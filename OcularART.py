import numpy as np
import ART
import pickle
import matplotlib.pyplot as plt


class OcularART:
    def __init__(self, FAM,num_spokes,spacing):
        self.FAM = FAM
        if type(num_spokes) != list:
            num_spokes = [num_spokes]
        self.num_spokes = num_spokes
        self.spacing = spacing
        self.spoke_angles = []
        for nspokes in num_spokes:
            self.spoke_angles.append([i*(2*np.pi/nspokes) for i in range(nspokes)])
        self.num_layers = len(num_spokes)
        self.wh = np.int(np.sqrt(FAM.ART_A.m))
        assert self.wh*self.wh == self.FAM.ART_A.m

    def image_slice(self,image,x,y,show_image=None,show_fill=255):
        x = np.round(x).astype(np.int16)
        y = np.round(y).astype(np.int16)

        h,w = image.shape[:2]
        x_a = x - (self.wh // 2)
        y_a = y - (self.wh // 2)
        x_b = x_a+self.wh
        y_b = y_a+self.wh

        if (x_a < 0 and x_b < 0) or (x_a >= h and x_b >= h):
            if show_image:
                return None, show_image
            else:
                return None
        if (y_a < 0 and y_b < 0) or (y_a >= w and y_b >= w):
            if show_image:
                return None, show_image
            else:
                return None
        oxa=0
        oya=0
        oxb=self.wh
        oyb=self.wh
        if x_a < 0:
            oxa = np.absolute(x_a)
            x_a = min(max(0, x_a),h)
        if y_a < 0:
            oya = np.absolute(y_a)
            y_a = min(max(0, y_a),w)
        if x_b > h:
            oxb = self.wh-(x_b-h)
            x_b = max(min(x_b, h),0)
        if y_b > w:
            oyb = self.wh-(y_b-w)
            y_b = max(min(y_b, w),0)
        img_out = np.zeros((self.wh,self.wh))
        try:
            # oxa,oya = oya,oxa
            # oxb, oyb = oyb, oxb
            # x_a, y_a = y_a, x_a
            # x_b, y_b = y_b, x_b
            img_out[oxa:oxb,oya:oyb] = image[x_a:x_b,y_a:y_b]
            if show_image is not None:
                show_image[x_a:x_b,y_a:y_b] = image[x_a:x_b,y_a:y_b]
                # show_image[x_a:x_b, y_a:y_b]  = show_fill
                # f88 = plt.figure(88)
                # f88.clf()
                # plt.hold = True
                # plt.imshow(show_image)
                # plt.scatter(y,x,color='b',marker='o')
                # f88.canvas.draw_idle()
                # plt.pause(0.1)
                return img_out, show_image
        except Exception as E:
            # print(E)
            # print(oxa,oxb,oya,oyb)
            # print(img_out[oxa:oxb,oya:oyb].shape,img_out.shape)
            # print(x_a,x_b,y_a,y_b)
            # print(image[x_a:x_b,y_a:y_b].shape)
            raise E
        return img_out

    def feedforward(self, image, x,y):
        assert image.shape[0] >= self.wh + 2*len(self.num_spokes)*self.spacing
        assert image.shape[1] >= self.wh + 2*len(self.num_spokes)*self.spacing

        center_image = self.image_slice(image,x,y)
        prediction = None
        center_A, center_R = self.FAM.activation(center_image,resonance=True)
        if np.max(center_R) > 0:
            prediction = np.argmax(center_R)
        else:
            layer = 0
            while layer < self.num_layers:
                spoke_angles = self.spoke_angles[layer]
                layer += 1
                spoke_images = [self.image_slice(image, x + layer*self.spacing * np.sin(ang), x + layer*self.spacing * np.cos(ang))
                    for ang in spoke_angles]
                spoke_ARs = [self.FAM.activation(spoke_image,resonance=True) if spoke_image is not None else [[0.],[0.]] for spoke_image in spoke_images]
                spoke_Rs = [AR[1] for AR in spoke_ARs]
                spoke_maxRs = [np.max(spoke_R) for spoke_R in spoke_Rs]
                if np.max(spoke_maxRs) > 0:
                    max_i = np.argmax(spoke_maxRs)
                    prediction = np.argmax(spoke_Rs[max_i])
                    return prediction

        return prediction

    def find_direction_of_category(self, image, x,y,c,show=False):
        assert image.shape[0] >= self.wh + 2*len(self.num_spokes)*self.spacing
        assert image.shape[1] >= self.wh + 2*len(self.num_spokes)*self.spacing
        if show:
            show_image = np.zeros_like(image)
        else:
            show_image = None
        if show_image is not None:
            # print('here')
            center_image,show_image = self.image_slice(image,x,y,show_image=show_image,show_fill=128)
            # plt.pause(1)
        else:
            center_image = self.image_slice(image, x, y)
        f99 = plt.figure(99)
        f99.clf()
        plt.imshow(center_image)
        f99.canvas.draw_idle()
        # print(x,y)
        prediction = None
        # center_A, center_R = self.FAM.activation(center_image,resonance=True)
        ffcm = self.FAM.feedforward(center_image,resonance=False)
        # print('FF:',ffcm)
        if ffcm == self.FAM.ulabels[c]:
            print("FOUND")
            # print(center_R)
            # plt.figure()
            # plt.imshow(center_image)
            return [None,1.,0.]
        else:
            layer = 0
            spoke_cos_activation = 0.
            spoke_sin_activation = 0.
            spc = 0
            max_sacs_global = -np.inf
            while layer < self.num_layers:
                spoke_angles = self.spoke_angles[layer]
                layer += 1
                if show_image is not None:
                    spoke_images = []
                    for ang in spoke_angles:
                        simg, show_image = self.image_slice(image, x + layer*self.spacing * np.cos(ang), y + layer*self.spacing * np.sin(ang), show_image=show_image)
                        spoke_images.append(simg)
                else:
                    spoke_images = [self.image_slice(image, x + layer*self.spacing * np.cos(ang), y + layer*self.spacing * np.sin(ang))
                                    for ang in spoke_angles]

                spoke_As = [self.FAM.activation(spoke_image,resonance=False) if spoke_image is not None else np.zeros((self.FAM.C,1)) for spoke_image in spoke_images]
                # spoke_Rs = [AR[1] for AR in spoke_ARs]
                spoke_Acs = [spoke_A[c] for spoke_A in spoke_As]
                max_sacs = np.max(spoke_Acs)
                if max_sacs > 0 and max_sacs > max_sacs_global:
                    max_sacs_i = np.argmax(spoke_Acs)
                    spoke_activation_angle = spoke_angles[max_sacs_i]
                    spoke_activation_level = max_sacs
                    max_sacs_global = max_sacs
                    # for spoke_A, ang in zip(spoke_As,spoke_angles):
                    #     spoke_cos_activation += spoke_A[c]*np.cos(ang)
                    #     spoke_sin_activation += spoke_A[c]*np.sin(ang)
                    #     spc += 1
        # spoke_cos_activation /= spc
        # spoke_sin_activation /= spc
        # spoke_activation_angle = np.arctan2(spoke_sin_activation, spoke_cos_activation)
        # spoke_activation_level = np.sqrt(spoke_sin_activation * spoke_sin_activation + spoke_cos_activation * spoke_cos_activation)
        if show:
            f88 = plt.figure(88)
            f88.clf()
            plt.imshow(show_image)
            f88.canvas.draw_idle()

        return [spoke_activation_angle,spoke_activation_level,layer]


    def find_directions(self, image, x,y):
        assert image.shape[0] >= self.wh + 4*self.spacing
        assert image.shape[1] >= self.wh + 4*self.spacing

        center_image = self.image_slice(image,x,y)
        prediction = None
        center_A, center_R = self.FAM.activation(center_image,resonance=True)
        if np.max(center_R) > 0:
            return [np.zeros_like(center_R),center_R,0.]
        else:
            layer = 0
            while layer < self.num_layers:

                spoke_angles = self.spoke_angles[layer]
                layer += 1
                spoke_images = [self.image_slice(image, x + self.spacing * np.sin(ang), x + self.spacing * np.cos(ang))
                    for ang in spoke_angles]
                spoke_As = [self.FAM.activation(spoke_image,resonance=False) for spoke_image in spoke_images]
                # spoke_As = [AR[0] for AR in spoke_ARs]

                spoke_maxAs = [np.max(spoke_A) for spoke_A in spoke_As]
                if np.max(spoke_maxAs) > 0:
                    spoke_cos_activation = np.zeros_like(spoke_As[0])
                    spoke_sin_activation = np.zeros_like(spoke_As[0])
                    for spoke_A, ang in zip(spoke_As,spoke_angles):
                        spoke_cos_activation += spoke_A*np.cos(ang)
                        spoke_sin_activation += spoke_A*np.sin(ang)
                    spoke_activation_angle = [np.arctan2(spoke_sin,spoke_cos) for spoke_sin, spoke_cos in zip(spoke_sin_activation, spoke_cos_activation)]
                    spoke_activation_level = [np.sqrt(spoke_sin*spoke_sin + spoke_cos*spoke_cos) for spoke_sin, spoke_cos in zip(spoke_sin_activation, spoke_cos_activation)]
                    return [spoke_activation_angle,spoke_activation_level,layer]

        return [np.zeros_like(center_A),np.zeros_like(center_A),-1]


    def activation_image(self,image,label):
        c = self.FAM.ulabels.index(label)
        aimg = np.zeros_like(image).astype(np.float)
        bimg = np.zeros_like(image).astype(np.float)
        for x in range(aimg.shape[0]):
            for y in range(aimg.shape[1]):
                # print(x,y,type(x),type(y),c,type(c))
                ang,act,layer = self.find_direction_of_category(image,x,y,c)
                aimg[x,y] = act
                if ang is not None:
                    bimg[x,y] = ang
                else:
                    bimg[x,y] = -np.inf
                # aimg[x,y] = self.FAM.activation(self.image_slice(image,x,y),resonance=False)[c]
        aimg -= np.min(aimg)
        aimg /= np.max(aimg)
        aimg = (255*aimg).astype(np.int)

        pickle.dump([aimg,bimg],open('ocular_activation.pckl','wb'))
        return aimg, bimg

