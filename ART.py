import numpy as np

def norm(x):
    return np.linalg.norm(x,1)

class FuzzyART:
    def __init__(self,rho=0.8,alpha=0.,beta=1.0):
        self.C = 0
        self.W = []
        self.m = 0
        self.rho = rho
        self.alpha = alpha
        self.beta = beta

    def compliment_code(self,sample):
        return np.concatenate([sample.flatten(),1-sample.flatten()])

    def _activation(self,x,c):
        return norm(np.minimum(x,self.W[c]))/(self.alpha+norm(self.W[c]))

    def _resonance(self, x, c):
        r = (norm(np.minimum(x, self.W[c])) / norm(x) ) >= self.rho
        return r

    def activation(self,x):
        return np.array([self._activation(x,c) for c in range(self.C)])

    def _learn(self,x,c):
        assert c <= self.C
        if c < self.C:
            self.W[c] = self.beta*np.minimum(self.W[c],x) + (1-self.beta)*self.W[c]
        else:
            self.W.append(None)
            self.W[c] = np.copy(x)
            self.C += 1

    def learn_sample(self,sample,reset_func=None):
        x = self.compliment_code(sample)
        if not self.m:
            self.m = len(sample.flatten())
        assert sum(x) == self.m
        if self.C:
            A = self.activation(x)
            Ai = list(range(self.C))
            Ai = [ai for _, ai in sorted(zip(A, Ai),reverse=True)]
            for ai in Ai:
                if self._resonance(x,ai):
                    if reset_func is None or not reset_func(ai):
                        self._learn(x,ai)
                        return ai
            self._learn(x,self.C)
        else:
            self.W.append(x)
            self.C = 1
        return self.C - 1

    def feedforward(self,sample,resonance=True):
        x = self.compliment_code(sample)
        assert sum(x) == self.m
        if self.C:
            A = self.activation(x)
            Ai = list(range(self.C))
            Ai = [ai for _, ai in sorted(zip(A, Ai), reverse=True)]
            for ai in Ai:
                if not resonance or self._resonance(x, ai):
                    return ai
            return -1
        else:
            return 0

    def resonant_activation(self,sample,reset_func=None,both=False):
        x = self.compliment_code(sample)
        assert sum(x) == self.m
        A = self.activation(x)
        B = np.copy(A)
        for i in range(self.C):
            if not self._resonance(x,i) or (reset_func is not None and reset_func(x,i)):
                B[i] = 0.
        if both:
            return A,B
        else:
            return B




class SimpleFuzzyARTMAP:
    def __init__(self,rho=0.8,alpha=0.,beta=1.0):
        self.ART_A = FuzzyART(rho,alpha,beta)
        self.C = 0
        self.map = dict()
        self.rmap = dict()
        self.ulabels = []

    def resonant_reset(self,c_a,c_b):
        if c_a in self.map:
            if c_b == self.map[c_a]:
                return False
            else:
                return True
        else:
            return False

    def train_sample(self,sample,label):
        if label not in self.ulabels:
            self.ulabels.append(label)
        if self.map:
            c_a = self.ART_A.learn_sample(sample,reset_func=lambda x: self.resonant_reset(x,label))
            assert c_a not in self.map or self.map[c_a] == label
            if c_a not in self.map:
                self.map[c_a] = self.ulabels.index(label)
                self.rmap[label] = set([c_a])
        else:
            c_a = self.ART_A.learn_sample(sample)
            self.map[c_a] = self.ulabels.index(label)
            self.rmap[label] = set([c_a])
        self.C = len(self.rmap)
        return c_a

    def label_lookup(self,label):
        return self.ulabels.index(label)

    def feedforward(self,sample, resonance=True):
        c_a = self.ART_A.feedforward(sample,resonance=resonance)
        # print('SFARTMAP:',c_a,resonance)
        if c_a < 0:
            return -1
        else:
            return self.ulabels[self.map[c_a]]

    def activation_matrix(self,sample,resonance=True):
        Amat = [[]]*self.C
        A, R = self.ART_A.resonant_activation(sample, both=True)
        for c_a, a in enumerate(A):
            c_b = self.map[c_a]
            Amat[c_b] = a
        if resonance:
            Bmat = [[]]*self.C
            for c_a,r in enumerate(R):
                c_b = self.map[c_a]
                Bmat[c_b] = r
            return Amat,Bmat
        return Amat


    def activation(self,sample,resonance=True):
        Amat, Rmat = self.activation_matrix(sample)
        A = np.array([np.max(Arow) for Arow in Amat])
        if resonance:
            B = np.array([np.max(Rrow) for Rrow in Rmat])
            # print('activation',len(A),len(B),self.C)
            return A,B
        return A












