from ART import SimpleFuzzyARTMAP
from OcularART import OcularART
from BitImage import make_dataset, ShowDataset
import random
import numpy as np
import matplotlib.pyplot as plt
import random
import pickle
from collections import deque

def shuffle_and_split_datset(X,Y,ratio=0.8):
    XY = list(zip(X,Y))
    random.shuffle(XY)
    n = len(XY)
    n_train = np.round(ratio*n).astype(np.int)
    XY_train = XY[:n_train]
    XY_test = XY[n_train:]
    return XY_train, XY_test


def train_ARTMAP(X,Y,epochs=10):
    FAM = SimpleFuzzyARTMAP(0.9,0.11,0.3)
    random.seed(101)
    train_data, test_data = shuffle_and_split_datset(X,Y,0.8)
    for epoch in range(epochs):
        random.shuffle(train_data)
        for x,y in train_data:
            FAM.train_sample(x,y)
        acc = 0
        for x,y in test_data:
            acc += float(y == FAM.feedforward(x,resonance=False))
        acc /= len(test_data)
        print('Accuracy after epoch {}: {}'.format(epoch,acc))
    return FAM

def search_image(image, OART,category,x=None,y=None,eps = 2.,disp=True):
    h,w = image.shape[:2]
    if x is None:
        x = float(w//2)
    if y is None:
        y = float(h//2)
    wh2 = OART.wh //2
    lvl = -1
    if disp:
        plt.figure(0)
        plt.hold =True
        plt.imshow(image)
        plt.scatter(y,x,marker='x',color='r')
    e = 0
    dx = 0
    dy = 0
    m = 0.7
    while lvl != 0:
        dx,dy = 0.,0.
        dir, act, lvl = OART.find_direction_of_category(image,x,y,OART.FAM.label_lookup(category),show=True)
        if act == 0:
            dx =  dx*m+(1-m)*np.random.random()
            dy =  dy*m+(1-m)*np.random.random()
        else:
            if act > 0.005:
                dx =  dx*m+(1-m)*3*np.sin(dir)
                dy =  dy*m+(1-m)*3*np.cos(dir)
                # dx = dx * m + (1 - m)  * (np.random.random() * 2 - 1)
                # dy = dy * m + (1 - m) * (np.random.random() * 2 - 1)
                print(act,dir,dx,dy)
            else:
                # dx =  dx*m+(1-m)*3*(np.random.random()*2 - 1)
                # dy =  dy*m+(1-m)*3*(np.random.random()*2 - 1)
                # x = np.random.randint(wh2,h-wh2)
                # y = np.random.randint(wh2,w-wh2)
                dx =  dx*m
                dy =  dy*m
                if abs(dx) < 0.1 and abs(dy) < 0.1:
                    dx =  (np.random.random()*2 - 1)
                    dy =  (np.random.random()*2 - 1)
                print('random')
        x += dx
        y += dy

        x = np.float(max(wh2,min(h-wh2,x)))
        y = np.float(max(wh2,min(w-wh2,y)))
        plt.figure(0)
        plt.scatter(y,x,marker='x',color='r')
        plt.pause(0.01)
        # print('step:',e)
        e+=1
    plt.show()
    return x,y

def show_samples(X,Y):
    known = []
    for x,y in zip(X,Y):
        if y not in known:
            known.append(y)
            plt.figure()
            plt.imshow(x)
    plt.show()


def load_and_display_activation_image():
    aimg, bimg = pickle.load(open('ocular_activation.pckl','rb'))
    plt.figure()
    plt.imshow(aimg)
    fig1, ax1 = plt.subplots()
    plt.hold = True
    s = 0.2
    for x in range(aimg.shape[0]):
        for y in range(aimg.shape[1]):
            ang = bimg[x,y]
            if not np.isinf(ang):
                a = s*aimg[x,y]
                ax1.quiver(y, x, a*np.sin(ang), -a*np.cos(ang), units='dots',scale=2.0)
    ax1.invert_yaxis()

def random_search(display=True,cr=False):
    if display:
        plt.figure()
        plt.hold = True
    aimg, bimg = pickle.load(open('ocular_activation.pckl', 'rb'))
    # aimg, bimg = pickle.load(open('ocular_activation.pckl', 'rb'))
    if display:
        plt.imshow(aimg)
    x = np.random.randint(0,aimg.shape[0])
    y = np.random.randint(0,aimg.shape[1])
    start_x = int(x)
    start_y = int(y)
    s = 1.0
    n = 0
    n_dist = 0.
    i_h = 0
    i_v = 0
    d_h = 2
    d_v = 2
    while True:
        n += 1
        d = bimg[x,y]
        a = aimg[x,y]

        if np.isinf(d):
            if display:
                print('FOUND',n)
                plt.scatter(x,y,marker='x',color='b')
            if cr:
                return n,n_dist/(np.sqrt((x-start_x)**2 + (y-start_y)**2)+0.001)
            else:
                return n
        rn = np.random.randint(0, 4)
        if rn == 0:
            i_h += 1
            dx = int(d_h)
            dy = 0
            d_h *= 2
        elif rn == 1:
            i_h += 1
            dx = -int(d_h)
            dy = 0
            d_h *= 2
        elif rn == 2:
            i_v += 1
            dx = 0
            dy = int(d_h)
            d_v *= 2
        elif rn == 3:
            i_v += 1
            dx = 0
            dy = int(d_h)
            d_v *= 2

        new_x = x + dx
        new_y = y + dy
        new_x = min(max(new_x,0),aimg.shape[1]-1)
        new_y = min(max(new_y,0),aimg.shape[0]-1)

        dx = new_x - x
        dy = new_y - y
        if (abs(dx) < 2 and abs(dy) < 2) or np.random.randint(0,4)==0:
            rdx = 0
            rdy = 0
            for hxy in history:
                if hxy:
                    hx, hy = hxy
                    if x != hx:
                        dhx = (x-hx)
                        rdx += 1/(dhx*dhx)
                    if y != hy:
                        dhy = (y-hy)
                        rdy += 1/(dhy*dhy)

            if abs(rdx) > 0 or abs(rdy) > 0:
                new_x += np.int(rdx*4)
                new_y += np.int(rdy*4)
                # print('random A', rdx, rdy)
            else:
                new_x += np.random.randint(-rstep,rstep+1)
                new_y += np.random.randint(-rstep,rstep+1)
                # print('random B', new_x, new_y)

        new_x = min(max(new_x, 0), aimg.shape[1] - 1)
        new_y = min(max(new_y, 0), aimg.shape[0] - 1)
        dx = new_x - x
        dy = new_y - y
        n_dist += np.sqrt(dx**2 + dy**2)
        if display:

            plt.plot([x,new_x],[y,new_y],'r-')
            plt.pause(0.01)
        x = new_x
        y = new_y
    if display:
        plt.show()

def pseudo_search(display=True,m=0.9,cr=False):
    if display:
        plt.figure()
        plt.hold = True
    aimg, bimg = pickle.load(open('ocular_activation.pckl', 'rb'))
    bimg = np.random.random(bimg.shape)*np.pi*2
    # aimg, bimg = pickle.load(open('ocular_activation.pckl', 'rb'))
    if display:
        plt.imshow(aimg)
    x = np.random.randint(0,aimg.shape[0])
    y = np.random.randint(0,aimg.shape[1])
    start_x = int(x)
    start_y = int(y)
    s = 1.0
    n = 0
    n_dist = 0.
    dxm = 0
    dym = 0
    rdx = 0
    rdy = 0
    phi=10
    # rstep = max(aimg.shape)
    rstep = 6
    history = deque([[]]*3,3)
    while True:
        history.appendleft([x,y])
        n += 1
        t_x = min(max(x + dxm, 0), aimg.shape[1] - 1)
        t_y = min(max(y + dym, 0), aimg.shape[0] - 1)
        # d = random.random()*np.pi*2
        d = bimg[t_x,t_y]
        a = aimg[x,y]
        # s = 0.2*a
        # print(x,s,d)
        if np.isinf(d) or n>=10000:
            if display:
                print('FOUND',n)
                plt.scatter(x,y,marker='x',color='b')
            n_dist += np.sqrt(dxm**2 + dym**2)
            if cr:
                return n,n_dist/(np.sqrt((x-start_x)**2 + (y-start_y)**2)+0.001)
            else:
                return n
        dx = np.int(s*np.sin(d))
        dy = np.int(s*np.cos(d))
        dxm = np.int(m*dxm + dx)
        # dxm = np.int(m*dxm + (1-m)*dx)
        dym = np.int(m*dym + dy)
        # dym = np.int(m*dym + (1-m)*dy)
        new_x = x + dxm
        new_y = y + dym
        new_x = min(max(new_x,0),aimg.shape[1]-1)
        new_y = min(max(new_y,0),aimg.shape[0]-1)

        dx = new_x - x
        dy = new_y - y
        if (abs(dx) < 2 and abs(dy) < 2) or np.random.randint(0,4)==0:
            rdx = 0
            rdy = 0
            for hxy in history:
                if hxy:
                    hx, hy = hxy
                    if x != hx:
                        dhx = (x-hx)
                        rdx += 1/(dhx*dhx)
                    if y != hy:
                        dhy = (y-hy)
                        rdy += 1/(dhy*dhy)

            if abs(rdx) > 0 or abs(rdy) > 0:
                new_x += np.int(rdx*phi)
                new_y += np.int(rdy*phi)
                # print('random A', rdx, rdy)
            else:
                new_x += np.random.randint(-rstep,rstep+1)
                new_y += np.random.randint(-rstep,rstep+1)
                # print('random B', new_x, new_y)

        new_x = min(max(new_x, 0), aimg.shape[1] - 1)
        new_y = min(max(new_y, 0), aimg.shape[0] - 1)
        dx = new_x - x
        dy = new_y - y
        n_dist += np.sqrt(dx**2 + dy**2)
        if display:

            plt.plot([x,new_x],[y,new_y],'r-')
            plt.pause(0.01)
        x = new_x
        y = new_y
    if display:
        plt.show()


def test_search():
    N = 500
    h = 0
    min_m = 0
    min_h = np.inf
    for m in np.arange(0.0,1.,0.02):
        for n in range(N):
            h += pseudo_search(display=False,m=m,cr=False)
        h /= N
        if h < min_h:
            min_h = h
            min_m = m
        print(m,h)
    print('MIN',min_m, min_h)
    pseudo_search(m=min_m)


def test_comp_ratio():
    N = 50
    min_m_h = 0
    min_m_tcr = 0
    min_h = np.inf
    min_tcr = np.inf
    for m in np.arange(0.0,1.,0.02):
        h = 0.
        tcr = 0.
        for n in range(N):
            dh,cr = pseudo_search(display=False,m=m,cr=True)
            h += dh
            tcr += cr
        h /= N
        tcr /= N
        if h < min_h:
            min_h = h
            min_m_h = m
        if tcr < min_tcr:
            min_tcr = tcr
            min_m_tcr = m
        print(m,h,tcr)
    print('MIN H',min_m_h, min_h)
    print('MIN CR',min_m_tcr, min_tcr)
    pseudo_search(m=min_m_tcr)

def test_comp_ratio2():
    N = 5000
    avg_n = 0
    avg_c = 0
    for n in range(N):
        h,c = pseudo_search(display=False,m=0.0,cr=True)
        avg_n += h
        avg_c += c
        print(n)
    avg_n /= N
    avg_c /= N
    print(avg_n,avg_c)

if __name__ == '__main__':
    test_comp_ratio2()
    # test_search()
    # load_and_display_activation_image()
    # [ocular_X, ocular_Y] = pickle.load(open('ocular_images.pckl', 'rb'))
    # plt.figure()
    # plt.imshow(ocular_X[0])
    # plt.show()
    # print(pseudo_search(m=0.6,display=False))
    input('waiting')
    exit()

    random.seed(101)
    np.random.seed(101)

    if True:
        X,Y = make_dataset(100,10,0.3,edge=2)
        # show_samples(X,Y)
        FAM = train_ARTMAP(X,Y,1)
        OART = OcularART(FAM,[8,16],4)
        pickle.dump(OART,open('ocularART.pckl','wb'))

    OART = pickle.load(open('ocularART.pckl','rb'))
    # ocular_X, ocular_Y = make_dataset(10,32)
    [ocular_X,ocular_Y] = pickle.load(open('ocular_images.pckl','rb'))
    OART.activation_image(ocular_X[0],ocular_Y[0])
    plt.figure()
    plt.imshow(ocular_X[0])
    load_and_display_activation_image()
    plt.show()

    exit()
    x,y = search_image(ocular_X[0],OART,ocular_Y[0],20,20)
    print(x,y)




