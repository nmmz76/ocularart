import numpy as np
from imageio import imread
import matplotlib.pyplot as plt
import os
import random

shape_dir = 'shapes/'
shape_files = [shape_dir+f for f in os.listdir(shape_dir) if f[-4:] =='.png']


class Shape:
    def __init__(self,image,name):
        self.img = image
        self.name = name


def load_shape(file_name):
    img = imread(file_name).mean(2).astype(np.bool)
    name =  file_name.split('/')[-1].replace('.png','')
    return Shape(img,name)

def generate_noise(imsize=64,p1=0.4):
    assert 0 <= p1 <= 1
    return np.random.choice([0,1],(imsize,imsize),p=[1-p1,p1])

def place_shape(shape,background,edge=0):
    sza = shape.img.shape[0]
    szb = shape.img.shape[1]
    edge_a = min(shape.img.shape[0],edge)
    edge_b = min(shape.img.shape[1],edge)
    if edge == 0:
        a = np.random.randint(background.shape[0]-shape.img.shape[0])
        b = np.random.randint(background.shape[1]-shape.img.shape[1])
    else:
        a = np.random.randint(-edge_a, background.shape[0] - shape.img.shape[0] +edge_a)
        b = np.random.randint(-edge_b, background.shape[1] - shape.img.shape[1] +edge_b)
    da0 = abs(min(0,a))
    a8 = a+sza
    da1 = abs(min(0,background.shape[0]-a8))
    da2 = shape.img.shape[0] - da1
    a8 = min(a8,background.shape[0])
    a = max(a,0)

    db0 = abs(min(0, b))
    b8 = b + szb
    db1 = abs(min(0, background.shape[1] - b8))
    db2 = shape.img.shape[1] - db1
    b8 = min(b8, background.shape[1])
    b = max(b, 0)
    try:
        background[a:a8,b:b8] = shape.img[da0:da2,db0:db2]
    except Exception as E:
        print(a,a8)
        print(b,b8)
        print(da0,da2)
        print(db0,db2)
        raise E
    return background

def make_image(shape,imsize=64,noise=0.3,edge=0):
    background = generate_noise(imsize=imsize,p1=noise)
    return place_shape(shape,background,edge)

def load_all_shapes():
    return [load_shape(shape_file) for shape_file in shape_files]

def make_dataset(n_per_shape=20,imsize=64,noise=0.3,shape_names = None, edge=0):
    assert imsize >= 8
    shapes = load_all_shapes()
    X = []
    Y = []
    for shape in shapes:
        if shape_names is None or shape.name in shape_names:
            for i in range(n_per_shape):
                X.append(make_image(shape,imsize=imsize,noise=noise,edge=edge))
                Y.append(shape.name)
    return X,Y


def ShowDataset():
    # x,y = make_dataset(4,imsize=10,noise=0.0,edge=4)
    x,y = make_dataset(4,32)
    print(len(x))
    print(len(shape_files))

    D = []
    H = []
    for i, (di,hi) in enumerate(zip(x,y)):
        if not i%4:
            D.append([])
            H.append([])
        D[-1].append(di)
        H[-1].append(hi)
    for dd,hh in zip(D,H):
        plt.figure()
        plt.title(hh[0])
        plt.subplot(2, 2, 1)
        plt.imshow(dd[0])
        plt.subplot(2, 2, 2)
        plt.imshow(dd[1])
        plt.subplot(2, 2, 3)
        plt.imshow(dd[2])
        plt.subplot(2, 2, 4)
        plt.imshow(dd[3])
    plt.show()

if __name__ == '__main__':
    ShowDataset()

