import numpy as np
import ART
import pickle
import matplotlib.pyplot as plt


class RetinalART:
    def __init__(self, FAM,num_spokes,spacing):
        self.FAM = FAM
        if type(num_spokes) != list:
            num_spokes = [num_spokes]
        self.num_spokes = num_spokes
        self.spacing = spacing
        spoke_angles = []
        for nspokes in num_spokes:
            spoke_angles.append([i*(2*np.pi/nspokes) for i in range(nspokes)])
        self.spoke_coordinates = [
            [
                [
                    np.round(layer * self.spacing * np.sin(ang)).astype(np.int16),
                    np.round(layer * self.spacing * np.cos(ang)).astype(np.int16)
                 ]
                for ang in layer_spoke_angles
            ]
            for layer, layer_spoke_angles in enumerate(spoke_angles)
            ]

        self.num_layers = len(num_spokes)
        self.wh = np.int(np.sqrt(FAM.ART_A.m))
        self.image_memory=[]
        assert self.wh*self.wh == self.FAM.ART_A.m

    def image_slice(self,image,x,y,show_image=None,show_fill=255):
        x = np.round(x).astype(np.int16)
        y = np.round(y).astype(np.int16)

        h,w = image.shape[:2]
        x_a = x - (self.wh // 2)
        y_a = y - (self.wh // 2)
        x_b = x_a+self.wh
        y_b = y_a+self.wh

        if (x_a < 0 and x_b < 0) or (x_a >= h and x_b >= h):
            if show_image:
                return None, show_image
            else:
                return None
        if (y_a < 0 and y_b < 0) or (y_a >= w and y_b >= w):
            if show_image:
                return None, show_image
            else:
                return None
        oxa=0
        oya=0
        oxb=self.wh
        oyb=self.wh
        if x_a < 0:
            oxa = np.absolute(x_a)
            x_a = min(max(0, x_a),h)
        if y_a < 0:
            oya = np.absolute(y_a)
            y_a = min(max(0, y_a),w)
        if x_b > h:
            oxb = self.wh-(x_b-h)
            x_b = max(min(x_b, h),0)
        if y_b > w:
            oyb = self.wh-(y_b-w)
            y_b = max(min(y_b, w),0)
        img_out = np.zeros((self.wh,self.wh))
        try:
            # oxa,oya = oya,oxa
            # oxb, oyb = oyb, oxb
            # x_a, y_a = y_a, x_a
            # x_b, y_b = y_b, x_b
            img_out[oxa:oxb,oya:oyb] = image[x_a:x_b,y_a:y_b]
            if show_image is not None:
                show_image[x_a:x_b,y_a:y_b] = image[x_a:x_b,y_a:y_b]
                # show_image[x_a:x_b, y_a:y_b]  = show_fill
                # f88 = plt.figure(88)
                # f88.clf()
                # plt.hold = True
                # plt.imshow(show_image)
                # plt.scatter(y,x,color='b',marker='o')
                # f88.canvas.draw_idle()
                # plt.pause(0.1)
                return img_out, show_image
        except Exception as E:
            # print(E)
            # print(oxa,oxb,oya,oyb)
            # print(img_out[oxa:oxb,oya:oyb].shape,img_out.shape)
            # print(x_a,x_b,y_a,y_b)
            # print(image[x_a:x_b,y_a:y_b].shape)
            raise E
        return img_out

    def feedforward(self, image, x,y, resonance = False):
        assert image.shape[0] >= self.wh + 2*len(self.num_spokes)*self.spacing
        assert image.shape[1] >= self.wh + 2*len(self.num_spokes)*self.spacing

        if not self.image_memory:
            self.image_memory = np.empty(image.shape,dtype=object)
        if self.image_memory[x,y] is None or self.image_memory[x,y]['activation'] is None:
            if self.image_memory[x, y] is None:
                self.image_memory[x, y] = {'activation': None, 'resonance': None, 'max_location': None}
            center_image = self.image_slice(image,x,y)
            if resonance:
                center_A, center_R = self.FAM.activation(center_image,resonance=True)
                self.image_memory[x, y]['resonance'] = center_R
            else:
                center_A = self.FAM.activation(center_image,resonance=False)
            global_A = np.copy(center_A)
            self.image_memory[x,y]['activation'] = center_A
            self.image_memory[x,y]['max_location'] = [[x,y]]*self.FAM.C
            for layer,spoke_xys in enumerate(self.spoke_coordinates):
                for sx,sy in spoke_xys:
                    if self.image_memory[x, y] is None:
                        self.image_memory[x, y] = {'activation': None, 'resonance': None, 'max_location': None}
                    if self.image_memory[sx,sy]['activation'] is  None:
                        self.image_memory[sx, sy]['activation'] = self.FAM.activation(self.image_slice(image,sx,sy),resonance=False)
                    Q = global_A < self.image_memory[sx,sy]['activation']
                    if any(Q):
                        global_A[Q] = self.image_memory[sx,sy]['activation'][Q]
                        for qi, q in enumerate(Q):
                            if q:
                                self.image_memory[x, y]['max_location'][qi] = [sx,sy]

        if not resonance:
            prediction = np.argmax(global_A)
        else:
            if np.any(self.image_memory[x,y]['resonance']):
                prediction = np.argmax(self.image_memory[x,y]['resonance'])
            else:
                prediction = -1
        return prediction

    def activation(self, image, x,y,c=None):
        assert image.shape[0] >= self.wh + 2 * len(self.num_spokes) * self.spacing
        assert image.shape[1] >= self.wh + 2 * len(self.num_spokes) * self.spacing

        if not self.image_memory:
            self.image_memory = np.empty(image.shape, dtype=object)

        if self.image_memory[x, y] is None:
            self.image_memory[x, y] = {'activation': None, 'resonance': None, 'max_location': None}
        elif self.image_memory[x,y]['activation'] is None and self.image_memory[x,y]['resonance'] is not None:
            if c is None:
                return [[[0, 0]] * len(self.image_memory[x, y]['resonance']), self.image_memory[x, y]['resonance']]
            else:
                return [[0, 0], self.image_memory[x, y]['resonance'][c]]
        center_image = self.image_slice(image, x, y)
        center_A, center_R = self.FAM.activation(center_image, resonance=True)
        self.image_memory[x, y]['resonance'] = center_R
        if c is not None and center_R[c]:
            return [[0,0],self.image_memory[x, y]['resonance'][c]]
        if c is None and np.all(center_R):
            return [[[0,0]]*len(center_R),self.image_memory[x, y]['resonance']]
        global_A = np.copy(center_A)
        self.image_memory[x, y]['activation'] = center_A
        self.image_memory[x, y]['max_location'] = [[x, y]] * self.FAM.C
        for layer, spoke_xys in enumerate(self.spoke_coordinates):
            for rx, ry in spoke_xys:
                sx = rx + x
                sy = ry + y
                if self.image_memory[x, y] is None:
                    self.image_memory[x, y] = {'activation': None, 'resonance': None, 'max_location': None}
                if self.image_memory[sx, sy]['activation'] is None:
                    self.image_memory[sx, sy]['activation'] = self.FAM.activation(self.image_slice(image, sx, sy),
                                                                                  resonance=False)
                Q = global_A < self.image_memory[sx, sy]['activation']
                if any(Q):
                    global_A[Q] = self.image_memory[sx, sy]['activation'][Q]
                    for qi, q in enumerate(Q):
                        if q:
                            self.image_memory[x, y]['max_location'][qi] = [sx, sy]
        if c is None:
            return [self.image_memory[x,y]['max_location'],global_A]
        else:
            return [self.image_memory[x,y]['max_location'][c],global_A[c]]


    def activation_image(self,image,label):
        c = self.FAM.ulabels.index(label)
        aimg = np.zeros_like(image).astype(np.float)
        bimg = np.zeros_like(image).astype(np.float)
        cimg = np.empty_like(image,dtype=object)
        for x in range(aimg.shape[0]):
            for y in range(aimg.shape[1]):
                # print(x,y,type(x),type(y),c,type(c))
                [x,y], act = self.activation(image,x,y,c)
                aimg[x,y] = act
                bimg[x,y] = np.atan2(x,y)
                cimg[x,y] = [x,y]
                # aimg[x,y] = self.FAM.activation(self.image_slice(image,x,y),resonance=False)[c]
        aimg -= np.min(aimg)
        aimg /= np.max(aimg)
        aimg = (255*aimg).astype(np.int)

        pickle.dump([aimg,bimg,cimg],open('ocular_activation.pckl','wb'))
        return aimg, bimg, cimg

